/*
讀取 (get)
    /address-book/:page?/:keyword?

新增 (get, post)
    /address-book/add
修改 (get, post)
    /address-book/edit/:id
刪除 (get, post)
    /address-book/remove/:id

*/

const express = require('express');
console.log('express.shin:', express.shin);


const mysql = require('mysql');
const bluebird = require('bluebird');
//資料庫帳號密碼
const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '1234',
    database: 'mytest'
});


db.connect();
bluebird.promisifyAll(db);
//------------------

const router = express.Router();

const perPage = 10;// 每頁幾筆資料


router.get('/:page?/:keyword?', (req, res) => {
    const output = {}; //設定一個物件
    output.params = req.params; //物件的params = 請求的params
    output.perPage = perPage;
    let page = parseInt(req.params.page) || 1;


    let keyword = req.params.keyword || ''; //如果keyword沒有設定 空字串
    let where = "WHERE 1"; //

    //如果keyword 有值
    if (keyword) {
        keyword = keyword.split("'").join("\\'"); // 設定跳脫  避免 SQL injection


        // where += " AND `name` LIKE '%" + keyword + "%' ";  //指查詢姓名
        where += " AND (`name` LIKE '%" + keyword + "%' or `address` LIKE '%" + keyword + "%') ";//查詢姓名與地址

        output.keyword = keyword; //顯示
    }


    // 算出有幾筆資料並命名 total
    let t_sql = "SELECT COUNT(1) `total` FROM `address_book`" + where;
    //
    db.queryAsync(t_sql)
        .then(results => {
            output.totalRows = results[0]['total'];
            output.totalPage = Math.ceil(output.totalRows / perPage);
            //查詢檔案如果為0 直接回傳
            if (output.totalPage === 0) {
                return;
            }
            if (page < 1) page = 1; //設定最小頁數
            if (page > output.totalPage) page = output.totalPage; //設定最大頁數
            output.page = page;
            return db.queryAsync("SELECT * FROM `address_book` " + where + " LIMIT ?, ? ", [(page - 1) * perPage, perPage])
            //return db.queryAsync("SELECT * FROM `address_book` LIMIT " + (page-1)*perPage + ", "+ perPage)
        })
        .then(results => {
            output.rows = results;
            res.json(output);
        })
        .catch(error => {
            console.log(error);
        })
});
module.exports = router;

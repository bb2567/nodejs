const express = require('express');
const url = require('url');
const bodyParser = require('body-parser');
const multer = require('multer'); //載入上傳(multer)模組
const upload = multer({ dest: 'tmp_uploads/' }); //設定上傳資料夾
const fs = require('fs'); //???
const session = require('express-session');//
const moment = require('moment-timezone');//載入時間插建
const mysql = require('mysql');
const bluebird =require('bluebird');
const cors = require('cors'); //允許所有主機來讀取
const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '1234',
    database: 'mytest'
});

db.connect(); 
bluebird.promisifyAll(db);  //?????


const app = express();
//const urlencodedParser = bodyParser.urlencoded({extended: false});




const whitelist = ['http://localhost:5000','http://localhost:3000']; //建立白名單
const corsOptions = {
    credentials: true,
    origin:function(origin,callback){
       // console.log('origin:' + origin);
        if(whitelist.indexOf(origin)>=0){
            callback(null, true);
        }else{
            callback(null,false)
        }
    }
};
app.use(cors(corsOptions)); //帶入 corsOptions 設定  預設 = *





app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(session({
    saveUninitialized:false,
    resave:false,
    secret:'Mark',       //加密
    cookie:{             //cookie 有其他可設定
        maxAge:1200000,  //20分鐘, 單位毫秒
    }
}));



app.set('view engine', 'ejs');

app.use(express.static('public'));

// routes 路由

app.get('/', (req, res) => {
    res.render('home', { name: 'mark song', a: 123 });

});

app.get('/b.html', (req, res) => {
    res.send(`<h2>hello world</h2>`);

});


app.get('/sales01', (req, res) => {
    const sales = require('./../data/sales01');
    //res.send(JSON.stringify(sales));
    //res.json(sales);
    res.render('sales01', {
        my_var: sales
    });
});

app.get('/try-qs', (req, res) => {
    const urlParts = url.parse(req.url, true);
    console.log(urlParts);

    res.render('try-qs', {
        query: urlParts.query
    });
});

app.get('/try-post-form', (req, res) => {
    res.render('try-post-form');
});
app.get('/try-post-form/123', (req, res) => {
    res.render('try-post-form');
});
app.post('/try-post-form', (req, res) => {
    res.render('try-post-form', req.body);


    //res.send(JSON.stringify(req.body));
});


app.get('/try-post-form2', (req, res) => {
    res.send('get: try-post-form2');
});
app.post('/try-post-form2', (req, res) => {
    res.json(req.body);
});
app.put('/try-post-form2', (req, res) => {
    res.send("PUT: try-post-form2");
});

app.get('/who/*/:name/:age?', (req, res) => {
    res.json(req.params);
});

app.get(/^\/09\d{2}\-?\d{3}\-?\d{3}$/, (req, res) => {
    let ss = req.url.slice(1); //從第一個開始
    ss = ss.split('?')[0];
    ss = ss.split('-').join('');
    res.send('手機: ' +ss);
});

//------------------------------------------------------------------------------
//模組化引入 方式1 = 不推薦
 const admin1 = require(__dirname+ '/admins/admin1');
 admin1(app);

//模組化引入 方式2 = 推薦
app.use(require(__dirname+'/admins/admin2'));

//模組化引入 方式3 = 
app.use('/sale',require(__dirname+'/admins/admin3'));


app.use('/address-book', require(__dirname+'/address_book'));


//---------------------------------------------------------------------------------------
app.post('/try_upload', upload.single('avatar'), (req, res) => {
    if (req.file && req.file.originalname) {
        console.log(req.file);
        switch (req.file.mimetype) { //類型
            case 'image/png':
            case 'image/jpeg':
                fs.createReadStream(req.file.path)
                    .pipe(
                        fs.createWriteStream('public/img/' + req.file.originalname)
                    );
                res.send('上傳成功');
                        break;
            default:
                return res.send('格式錯誤');
        } 
    }else {
            res.send('上傳失敗');
    
    }
});


app.get('/try-session',(req,res)=>{
    req.session.my_views = req.session.my_views || 0;
    req.session.my_views++;
     res.json({
        aa: 'hello',
        'my views': req.session.my_views
    });

});
//時間
 app.get('/try-time',(req, res)=>{
const fm = 'YYYY-MM-DD HH:mm:ss';
const mo1 = moment(req.session.cookie.expires);
const mo2 = moment(new Date());
res.contentType('text/plain');
res.write(new Date()+"\n");
res.write('台北' + mo1.format(fm)+"\n");
res.write(mo1.tz('Europe/London').format(fm)+"\n");
res.write(mo2.tz('Asia/Tokyo').format(fm) + "\n");
res.write(mo1.constructor.name + "\n")

res.end();

 });


//連線設定 1 加篩選
app.get('/try-db', (req, res)=> {
    const sql = "SELECT * FROM `address_book` WHERE `name` LIKE ? ";
    db.query(sql, [ "%小明%" ], (error, results, fields)=>{
        console.log(error);
        console.log(results);
        console.log(fields);
        //res.json(results);

        for(let r of results){
            r.birthday2 = moment(r.birthday).format('YYYY-MM-DD');
        }
        res.render('try-db',{
            rows: results
        });
    });
    //res.send('ok');
});




app.get('/try-db2/:page?', (req, res)=> {
    let page = req.params.page || 1;
    let perPage = 5;
    const output = {};

    db.queryAsync("SELECT COUNT(1) total FROM `address_book`")
        .then(results=>{
            //res.json(results);
            output.總筆數 = results[0].total;
            return db.queryAsync(`SELECT * FROM address_book LIMIT ${(page-1)*perPage}, ${perPage}`);
        })
        .then(results=>{
            output.rows = results;
            res.json(output);
        })
        .catch(error=>{
            console.log(error);
            res.send(error);
        });
});





app.use((req, res) => {
    res.type('html');
    res.status(404);
    res.render('404');
});

app.listen(3000, () => {
    console.log('server started 3000');
});







